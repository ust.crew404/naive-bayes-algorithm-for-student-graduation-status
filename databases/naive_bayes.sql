-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 16, 2020 at 07:55 PM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naive_bayes`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_testing`
--

CREATE TABLE `tbl_testing` (
  `id` int(11) NOT NULL,
  `tbl_jk` varchar(20) NOT NULL,
  `status_mhs` varchar(20) NOT NULL,
  `status_pernikahan` varchar(20) NOT NULL,
  `ips_mhs` varchar(5) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_testing`
--

INSERT INTO `tbl_testing` (`id`, `tbl_jk`, `status_mhs`, `status_pernikahan`, `ips_mhs`, `status`) VALUES
(1, 'Laki - Laki', 'Mahasiswa', 'Belum', '2.70', 'Terlambat'),
(2, 'Laki - Laki', 'Bekerja', 'Belum', '2.75', 'Terlambat');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_training`
--

CREATE TABLE `tbl_training` (
  `id` int(11) NOT NULL,
  `tbl_jk` varchar(20) NOT NULL,
  `status_mhs` varchar(15) NOT NULL,
  `status_pernikahan` varchar(20) NOT NULL,
  `ips_mhs` varchar(5) NOT NULL,
  `status_kelulusan` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_training`
--

INSERT INTO `tbl_training` (`id`, `tbl_jk`, `status_mhs`, `status_pernikahan`, `ips_mhs`, `status_kelulusan`) VALUES
(1, 'Laki - Laki', 'Mahasiswa', 'Belum', '3.06', 'Tepat'),
(2, 'Laki - Laki', 'Bekerja', 'Belum', '3.19', 'Tepat'),
(3, 'Perempuan', 'Mahasiswa', 'Belum', '3.30', 'Tepat'),
(4, 'Perempuan', 'Mahasiswa', 'Menikah', '3.01', 'Tepat'),
(5, 'Laki - Laki', 'Bekerja', 'Menikah', '2.50', 'Tepat'),
(6, 'Laki - Laki', 'Bekerja', 'Menikah', '3.00', 'Terlambat'),
(7, 'Perempuan', 'Bekerja', 'Menikah', '2.70', 'Terlambat'),
(8, 'Perempuan', 'Bekerja', 'Belum', '2.40', 'Terlambat'),
(9, 'Laki - Laki', 'Bekerja', 'Belum', '2.50', 'Terlambat'),
(10, 'Perempuan', 'Mahasiswa', 'Menikah', '2.50', 'Terlambat'),
(11, 'Perempuan', 'Mahasiswa', 'Belum', '2.50', 'Terlambat'),
(12, 'Perempuan', 'Mahasiswa', 'Belum', '3.50', 'Tepat'),
(13, 'Laki - Laki', 'Bekerja', 'Menikah', '3.50', 'Tepat'),
(14, 'Laki - Laki', 'Mahasiswa', 'Menikah', '3.25', 'Tepat'),
(15, 'Laki - Laki', 'Mahasiswa', 'Belum', '2.75', 'Terlambat'),
(16, 'Laki - Laki', 'Mahasiswa', 'Belum', '2.70', 'Terlambat');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `username` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `fullname` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id`, `username`, `password`, `fullname`) VALUES
(1, 'demo', 'ed0de7252acf2980e677bacab01bde25', 'Programmer Gila');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_testing`
--
ALTER TABLE `tbl_testing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_training`
--
ALTER TABLE `tbl_training`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_testing`
--
ALTER TABLE `tbl_testing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_training`
--
ALTER TABLE `tbl_training`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
