<?php
class Dashboard extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('login') != 'login')
				{
					redirect(base_url());
				}
			}
	function index(){
			$data['title'] = "Dashboard";
			$content['training'] = $this->db->get('tbl_training')->num_rows();
			// $content['history'] = $this->db->get('tbl_k')->num_rows();
			$data['content'] = $this->load->view('dashboard/content',$content,true);
			$this->load->view('dashboard/index',$data);
		}
}
?>