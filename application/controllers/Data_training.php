<?php
// require('./application/third_party/phpoffice/vendor/autoload.php');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
class Data_training extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('login') != 'login')
				{
					redirect(base_url());
				}
			}
	function index(){
			$data['title'] = "Data Training";
			$testing['data'] = $this->db->get('tbl_training')->result();
			$data['content'] = $this->load->view('dashboard/table_training',$testing,true);
			$this->load->view('dashboard/index',$data);
		}
	public function upload_excel()
    {
    	error_reporting(0);
        // Load plugin PHPExcel nya
        include APPPATH.'third_party/PHPExcel/PHPExcel.php';

        $config['upload_path'] = realpath('assets/excel');
        $config['allowed_types'] = 'xlsx|xls|csv';
        $config['max_size'] = '10000';
        $config['encrypt_name'] = true;

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('userfile')) {

            //upload gagal
            $this->session->set_userdata('notif', '<script type="text/javascript">
        swal("Oops!", "Upload File Gagal", "error");
      </script>');
            //redirect halaman
            redirect('data_training');

        } else {

            $data_upload = $this->upload->data();

            $excelreader     = new PHPExcel_Reader_Excel2007();
            $loadexcel         = $excelreader->load('assets/excel/'.$data_upload['file_name']); // Load file yang telah diupload ke folder excel
            $sheet             = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $data = array();

            $numrow = 0;
            foreach($sheet as $row){
                            if($numrow > 0){
                                array_push($data, array(
                                    'tbl_jk' => $row['A'],
                                    'status_mhs'      => $row['B'],
                                    'status_pernikahan'      => $row['C'],
                                    'ips_mhs'      => $row['D'],
                                    'status_kelulusan'      => $row['E'],
                                ));
                    		}
               $numrow++;
            }
            // var_dump($data);
            // $index = count($data) - 1;
            // unset($data[$index]);
            $this->db->insert_batch('tbl_training', $data);
            //delete file from server
            unlink(realpath('assets/excel/'.$data_upload['file_name']));

            //upload success
            $this->session->set_userdata('notif', '<script type="text/javascript">
        swal("Bagus!", "Upload File Berhasil", "success");
      </script>');
            //redirect halaman
            redirect('data_training');

        }
    }
    public function tambah_data(){
    	$data = array(
                                    'tbl_jk' => $this->input->post('tbl_jk'),
                                    'status_mhs'      => $this->input->post('status_mhs'),
                                    'status_pernikahan'      => $this->input->post('status_pernikahan'),
                                    'ips_mhs'      => $this->input->post('ips_mhs'),
                                    'status_kelulusan'      => $this->input->post('status_kelulusan')
                                );
    	$this->db->insert('tbl_training', $data);
    	$this->session->set_userdata('notif', '<script type="text/javascript">
        swal("Bagus!", "Data Berhasil Ditambahkan", "success");
      </script>');
    	redirect('data_training');
    }
    public function hapus($id){
    	$this->db->delete('tbl_training', array('id'=>$id));
    	$this->session->set_userdata('notif', '<script type="text/javascript">
        swal("Bagus!", "Data Berhasil Dihapus", "success");
      </script>');
    	redirect('data_training');
    }
    public function edit($id=''){
    	if ($id != '') {
    		# code...
    		$data['title'] = "Edit Data Training";
			$testing['data'] = $this->db->get_where('tbl_training',array('id' => $id))->result();
			$data['content'] = $this->load->view('dashboard/edit_training',$testing,true);
			$this->load->view('dashboard/index',$data);
    	}else{
    		redirect('data_training');
    	}
    }
    public function update_data(){
    	$where = array('id' => $this->input->post('id'));
    	$data = array(
                                    'tbl_jk' => $this->input->post('tbl_jk'),
                                    'status_mhs'      => $this->input->post('status_mhs'),
                                    'status_pernikahan'      => $this->input->post('status_pernikahan'),
                                    'ips_mhs'      => $this->input->post('ips_mhs'),
                                    'status_kelulusan'      => $this->input->post('status_kelulusan')
                                );
    	$this->db->update('tbl_training', $data, $where);
    	$this->session->set_userdata('notif', '<script type="text/javascript">
        swal("Bagus!", "Data Berhasil Diubah", "success");
      </script>');
    	redirect('data_training');
    }
    public function export()
     {
          $semua_data = $this->db->get('tbl_training')->result();

          $spreadsheet = new Spreadsheet;

          $spreadsheet->setActiveSheetIndex(0)
                      ->setCellValue('A1', 'JK')
                      ->setCellValue('B1', 'Status Mhs')
                      ->setCellValue('C1', 'Status Pernikah')
                      ->setCellValue('D1', 'IPK 1-6')
                      ->setCellValue('E1', 'Status Kelulusan');

          $kolom = 2;
          foreach($semua_data as $p) {

               $spreadsheet->setActiveSheetIndex(0)
                           ->setCellValue('A' . $kolom, $p->tbl_jk)
                           ->setCellValue('B' . $kolom, $p->status_mhs)
                           ->setCellValue('C' . $kolom, $p->status_pernikahan)
                           ->setCellValue('D' . $kolom, $p->ips_mhs)
                           ->setCellValue('E' . $kolom, $p->status_kelulusan);

               $kolom++;

          }

          $writer = new Xlsx($spreadsheet);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="data_training.xls"');
        header('Cache-Control: max-age=0');

      $writer->save('php://output');
     }
}
?>