<?php
class Data_testing extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('login') != 'login')
				{
					redirect(base_url());
				}
			}
	function index(){
			$data['title'] = "Data Testing";
			$data['content'] = $this->load->view('dashboard/table_testing',[],true);
			$this->load->view('dashboard/index',$data);
		}
	public function proses()
    {
        $data = array(
                            'tbl_jk' => $this->input->post('tbl_jk'),
                            'status_mhs'      => $this->input->post('status_mhs'),
                            'status_pernikahan'      => $this->input->post('status_pernikahan'),
                            'ips_mhs'      => $this->input->post('ips_mhs')
                        );
        $this->session->set_userdata($data);
        $kelamin = $this->session->userdata('tbl_jk');
        $status_mhs = $this->session->userdata('status_mhs');
        $status_pernikahan = $this->session->userdata('status_pernikahan');
        $ips_mhs = $this->session->userdata('ips_mhs');
        $data['title'] = "Proses Perhitungan";
        $proses['training'] = $this->db->get('tbl_training')->num_rows();

        // pencarian P(Ci)
        $proses['PCi1'] = $this->db->get_where('tbl_training', array('status_kelulusan' => 'Tepat'))->num_rows();
        $proses['PCi2'] = $this->db->get_where('tbl_training', array('status_kelulusan' => 'Terlambat'))->num_rows();

        // Pencarian P(X|Ci)
        $proses['PXCi1'] = $this->db->get_where('tbl_training', array('tbl_jk' => $kelamin, 'status_kelulusan' => 'Tepat'))->num_rows();
        $proses['PXCi2'] = $this->db->get_where('tbl_training', array('tbl_jk' => $kelamin, 'status_kelulusan' => 'Terlambat'))->num_rows();

        // Pencarian P(X|Ci)
        $proses['PXCi3'] = $this->db->get_where('tbl_training', array('status_mhs' => $status_mhs, 'status_kelulusan' => 'Tepat'))->num_rows();
        $proses['PXCi4'] = $this->db->get_where('tbl_training', array('status_mhs' => $status_mhs, 'status_kelulusan' => 'Terlambat'))->num_rows();

        // Pencarian P(X|Ci)
        $proses['PXCi5'] = $this->db->get_where('tbl_training', array('status_pernikahan' => $status_pernikahan, 'status_kelulusan' => 'Tepat'))->num_rows();
        $proses['PXCi6'] = $this->db->get_where('tbl_training', array('status_pernikahan' => $status_pernikahan, 'status_kelulusan' => 'Terlambat'))->num_rows();

        // Pencarian P(X|Ci)
        $proses['PXCi7'] = $this->db->get_where('tbl_training', array('ips_mhs' => $ips_mhs, 'status_kelulusan' => 'Tepat'))->num_rows();
        $proses['PXCi8'] = $this->db->get_where('tbl_training', array('ips_mhs' => $ips_mhs, 'status_kelulusan' => 'Terlambat'))->num_rows();

        $this->session->set_userdata('notif', '<script type="text/javascript">
        swal("Keren!", "Proses Perhitungan Selesai ^_^", "success");
      </script>');

        $data['content'] = $this->load->view('dashboard/proses',$proses,true);
        $this->load->view('dashboard/index',$data);
    }
}
?>