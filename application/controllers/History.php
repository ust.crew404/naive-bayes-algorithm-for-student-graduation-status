<?php
class History extends CI_Controller
{
	
	function __construct()
			{
				parent::__construct();
				if($this->session->userdata('login') != 'login')
				{
					redirect(base_url());
				}
			}
	function index(){
			$data['title'] = "Daftar Data Testing";
			$content['history'] = $this->db->get('tbl_testing')->result();
			$data['content'] = $this->load->view('dashboard/table_history',$content,true);
			$this->load->view('dashboard/index',$data);
		}
	function hapus($id){
    	$this->db->delete('tbl_testing', array('id'=>$id));
    	$this->session->set_userdata('notif', '<script type="text/javascript">
        swal("Bagus!", "Data Berhasil Dihapus", "success");
      </script>');
    	redirect('data_training');
    }
    function export(){
    	$q = $this->db->get('tbl_testing')->result();
    	// $r= 0;
    	foreach ($q as $q) {
    			$data = array(
    							'tbl_jk' => $q->tbl_jk,
    							'status_mhs' => $q->status_mhs,
    							'status_pernikahan' => $q->status_pernikahan,
    							'ips_mhs' => $q->ips_mhs,
    							'status_kelulusan' => $q->status
    						 );
    			$this->db->insert('tbl_training', $data);
    	}
    	$this->session->set_userdata('notif', '<script type="text/javascript">
		        swal("Bagus!", "Data Berhasil Dijadikan Data Training", "success");
		      </script>');
    	redirect('history');
    }
}
?>