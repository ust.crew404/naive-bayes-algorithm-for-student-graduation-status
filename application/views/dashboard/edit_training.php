        <div class="container-fluid">
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Training</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Edit Data Training</h6>
            </div>
            <div class="card-body">
            <form action="<?php echo base_url('data_training/update_data');?>" method="POST">
            <table width="100%">
            <?php foreach ($data as $d): ?>
              <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td><select class="form-control form-control-user" name="tbl_jk" required="">
                  <option selected="" value="<?php echo $d->tbl_jk; ?>"><?php echo $d->tbl_jk; ?></option>
                  <option value="Laki - Laki">Laki - Laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select>
                <input type="number" name="id" value="<?php echo $d->id; ?>" hidden=""></td>
              </tr>
              <tr>
                <td>Status Mahasiswa</td>
                <td>:</td>
                <td><select class="form-control form-control-user" name="status_mhs" required="">
                  <option selected="" value="<?php echo $d->status_mhs; ?>"><?php echo $d->status_mhs; ?></option>
                  <option value="Mahasiswa">Mahasiswa</option>
                  <option value="Bekerja">Bekerja</option>
                </select></td>
              </tr>
              <tr>
                <td>Status Pernikahan</td>
                <td>:</td>
                <td><select class="form-control form-control-user" name="status_pernikahan" required="">
                  <option selected="" value="<?php echo $d->status_pernikahan; ?>"><?php echo $d->status_pernikahan; ?></option>
                  <option value="Belum">Belum</option>
                  <option value="Menikah">Menikah</option>
                </select></td>
              </tr>
              <tr>
                <td>IPK Semester 1-6</td>
                <td>:</td>
                <td><input type="text" name="ips_mhs" value="<?php echo $d->ips_mhs; ?>" class="form-control form-control-user" placeholder="IPK Semester 1-6" required=""></td>
              </tr>
              <tr>
                <td>Jumlah Pegawai</td>
                <td>:</td>
                <td><select class="form-control form-control-user" name="status_kelulusan" required="">
                  <option selected="" value="<?php echo $d->status_kelulusan; ?>"><?php echo $d->status_kelulusan; ?></option>
                  <option value="Tepat">Tepat</option>
                  <option value="Terlambat">Terlambat</option>
                </select></td>
              </tr>
              <tr>
                <td colspan="3"><br><center><input type="submit" class="btn btn-primary btn-user btn-block" value="Update Data"></center></td>
              </tr>
            <?php endforeach ?>
            </table>
            </form>
              <hr>
            </div>
          </div>

        </div>
        <!-- /.container-fluid