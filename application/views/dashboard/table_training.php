        <div class="container-fluid">
        <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
        ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Training</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Daftar Data Training</h6>
            </div>
            <div class="card-body">
              <form action="<?php echo base_url('data_training/upload_excel');?>" method="POST" enctype="multipart/form-data">
              <table width="50%">
                <tr>
                  <td><input type="file" name="userfile" class="form-control form-control-user" accept=".xls, .xlsx"></td>
                  <td><input type="submit" class="btn btn-primary btn-user btn-block" value="Upload"></td>
                  <td><a href="<?php echo base_url('assets/excel/')?>Format_upload.xlsx" target="_blank" class="btn btn-success btn-user btn-block">Download Format</a></td>
                  <td><a href="<?php echo base_url().'data_training/export/'; ?>" class="btn btn-primary btn-icon-split">
                            <span class="text">Download Data</span>
                          </a></td>
                </tr>
              </table>
            </form>
            <br>
            <form action="<?php echo base_url('data_training/tambah_data');?>" method="POST">
            <table width="100%">
              <tr>
                <td><select class="form-control form-control-user" name="tbl_jk" required="">
                  <option selected="" disabled="">-- Pilih --</option>
                  <option value="Laki - Laki">Laki - Laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select></td>
                <td><select class="form-control form-control-user" name="status_mhs" required="">
                  <option selected="" disabled="">-- Pilih --</option>
                  <option value="Mahasiswa">Mahasiswa</option>
                  <option value="Bekerja">Bekerja</option>
                </select></td>
                <td><select class="form-control form-control-user" name="status_pernikahan" required="">
                  <option selected="" disabled="">-- Pilih --</option>
                  <option value="Belum">Belum</option>
                  <option value="Menikah">Menikah</option>
                </select></td>
                <td><input type="text" name="ips_mhs" class="form-control form-control-user" placeholder="IPK Semester 1-6" required=""></td>
                <td><select class="form-control form-control-user" name="status_kelulusan" required="">
                  <option selected="" disabled="">-- Pilih --</option>
                  <option value="Tepat">Tepat</option>
                  <option value="Terlambat">Terlambat</option>
                </select></td>
                <td><input type="submit" class="btn btn-primary btn-user btn-block" value="Tambah Data"></td>
              </tr>
            </table>
            </form>
              <hr>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Jenis Kelamin</th>
                    <th>Status Mahasiswa</th>
                    <th>Status Pernikahan</th>
                    <th>IPK Semester - 6</th>
                    <th>Status Kelulusan</th>
                    <th></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>Jenis Kelamin</th>
                    <th>Status Mahasiswa</th>
                    <th>Status Pernikahan</th>
                    <th>IPK Semester 1 - 6</th>
                    <th>Status Kelulusan</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php $no=1; foreach ($data as $d): ?>
                    <tr>
                      <td><center><?php echo $no++; ?></center></td>
                      <td><center><?php echo $d->tbl_jk ?></center></td>
                      <td><center><?php echo $d->status_mhs ?></center></td>
                      <td><center><?php echo $d->status_pernikahan ?></center></td>
                      <td><center><?php echo $d->ips_mhs ?></center></td>
                      <td><center><?php echo $d->status_kelulusan ?></center></td>
                      <td><a href="<?php echo base_url().'data_training/edit/'.$d->id; ?>" class="btn btn-warning btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Edit</span>
                          </a>
                          <a onclick="return confirm('Yakin ingin menghapus data ini?')" href="<?php echo base_url().'data_training/hapus/'.$d->id; ?>" class="btn btn-danger btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-trash"></i>
                            </span>
                            <span class="text">Hapus</span>
                          </a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid