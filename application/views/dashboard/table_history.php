        <div class="container-fluid">
        <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
        ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">History</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">History Data Testing</h6>
            </div>
            <div class="card-body">
                <a onclick="return confirm('Yakin ingin menjadikan hasil perhitungan ke Data Training?')" href="<?php echo base_url().'history/export/'; ?>" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Jadikan Data Training</span>
                          </a> 
              <hr>
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Jenis Kelamin</th>
                    <th>Status Mahasiswa</th>
                    <th>Status Pernikahan</th>
                    <th>IPK Semester 1 - 6</th>
                    <th>Status Kelulusan</th>
                    <th></th>
                  </tr>
                </thead>
                <tfoot>
                  <tr>
                    <th>No.</th>
                    <th>Jenis Kelamin</th>
                    <th>Status Mahasiswa</th>
                    <th>Status Pernikahan</th>
                    <th>IPK Semester 1 - 6</th>
                    <th>Status Kelulusan</th>
                    <th></th>
                  </tr>
                </tfoot>
                <tbody>
                  <?php $no=1; foreach ($history as $d): ?>
                    <tr>
                      <td><center><?php echo $no++; ?></center></td>
                      <td><center><?php echo $d->tbl_jk ?></center></td>
                      <td><center><?php echo $d->status_mhs ?></center></td>
                      <td><center><?php echo $d->status_pernikahan ?></center></td>
                      <td><center><?php echo $d->ips_mhs ?></center></td>
                      <td><center><?php echo $d->status ?></center></td>
                      <td>
                          <a onclick="return confirm('Yakin ingin menghapus data ini?')" href="<?php echo base_url().'data_training/hapus/'.$d->id; ?>" class="btn btn-danger btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-trash"></i>
                            </span>
                            <span class="text">Hapus</span>
                          </a>
                      </td>
                    </tr>
                  <?php endforeach ?>
                </tbody>
                </table>
              </div>
            </div>
          </div>

        </div>
        <!-- /.container-fluid