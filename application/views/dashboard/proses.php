        <div class="container-fluid">
        <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
        ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Testing</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Proses Perhitungan Naive Bayes</h6><a href="<?php echo base_url().'data_testing'; ?>" class="btn btn-primary btn-icon-split">
                            <span class="icon text-white-50">
                              <i class="fas fa-edit"></i>
                            </span>
                            <span class="text">Input Data Testing</span>
                          </a>
            </div>
            <div class="card-body">
            <table>
              <tr>
              <td colspan="3"><b>P(Ci)=</b><hr></td>
            </tr>
            <tr>
              <td>P(STATUS KELULUSAN = “TEPAT”)</td>
              <td>=</td>
              <td><?php echo $PCi1." / ".$training; ?></td>
            </tr>
            <tr>
              <td>P(STATUS KELULUSAN = “TERLAMBAT”)</td>
              <td>=</td>
              <td><?php echo $PCi2." / ".$training; ?></td>
            </tr>
            <tr>
              <td colspan="3"><hr></td>
            </tr>
            <tr>
              <td colspan="3"><b>P(X|Ci)=</b><hr></td>
            </tr>
            <tr>
              <td>P(JENIS KELAMIN = “<?php echo $this->session->userdata('tbl_jk'); ?>” | STATUS KELULUSAN = “TEPAT”) </td>
              <td> = </td>
              <td><?php echo $PXCi1." / ".$PCi1; ?></td>
            </tr>
            <tr>
              <td>P(STATUS MAHASISWA = “<?php echo $this->session->userdata('status_mhs'); ?>” | STATUS KELULUSAN = “TEPAT”) </td>
              <td> = </td>
              <td><?php echo $PXCi3." / ".$PCi1; ?></td>
            </tr>
            <tr>
              <td>P(STATUS PERNIKAHAN = “<?php echo $this->session->userdata('status_pernikahan'); ?>” | STATUS KELULUSAN = “TEPAT”) </td>
              <td> = </td>
              <td><?php echo $PXCi5." / ".$PCi1; ?></td>
            </tr>
            <tr>
              <td>P(STATUS PERNIKAHAN = “<?php echo $this->session->userdata('ips_mhs'); ?>” | STATUS KELULUSAN = “TEPAT”) </td>
              <td> = </td>
              <td><?php echo $PXCi7." / ".$PCi1; ?></td>
            </tr>
            <tr>
              <td colspan="3"><hr></td>
            </tr>
            <tr>
              <td>P(JENIS KELAMIN = “<?php echo $this->session->userdata('tbl_jk'); ?>” | STATUS KELULUSAN = “TERLAMBAT”) </td>
              <td> = </td>
              <td><?php echo $PXCi2." / ".$PCi2; ?></td>
            </tr>
            <tr>
              <td>P(STATUS MAHASISWA = “<?php echo $this->session->userdata('status_mhs'); ?>” | STATUS KELULUSAN = “TERLAMBAT”) </td>
              <td> = </td>
              <td><?php echo $PXCi4." / ".$PCi2; ?></td>
            </tr>
            <tr>
              <td>P(STATUS PERNIKAHAN = “<?php echo $this->session->userdata('status_pernikahan'); ?>” | STATUS KELULUSAN = “TERLAMBAT”) </td>
              <td> = </td>
              <td><?php echo $PXCi6." / ".$PCi2; ?></td>
            </tr>
            <tr>
              <td>P(STATUS PERNIKAHAN = “<?php echo $this->session->userdata('ips_mhs'); ?>” | STATUS KELULUSAN = “TERLAMBAT”) </td>
              <td> = </td>
              <td><?php echo $PXCi8." / ".$PCi2; ?></td>
            </tr>
            <tr>
              <td colspan="3"><hr></td>
            </tr>
            </table>
            <table width="100%">
             <tr>
                <td>P(X|<b>STATUS KELULUSAN</b> = “TEPAT”)</td>
                <td>= </td>
                <td>P(<b>KELAMIN</b> = “<?php echo $this->session->userdata('tbl_jk'); ?>”, <b>STATUS MAHASISWA</b> = “<?php echo $this->session->userdata('status_mhs'); ?>”, <b>STATUS PERNIKAHAN</b> = “<?php echo $this->session->userdata('status_pernikahan'); ?>”, <b>IPK</b> = <?php echo $this->session->userdata('ips_mhs'); ?> | <b>STATUS KELULUSAN</b> = “TEPAT”)</td>
              </tr>
              <tr>
                <td></td>
                <td>=</td>
                <td><?php echo $PXCi1." / ".$PCi1." x ".$PXCi3." / ".$PCi1." x ".$PXCi5." / ".$PCi1." x ".$PXCi7." / ".$PCi1; ?></td>
              </tr>
              <tr>
                <td></td>
                <td>=</td>
                <td><?php $jumlah = 0;
                $jumlah = ($PXCi1 / $PCi1) * ($PXCi3 / $PCi1) * ($PXCi5 / $PCi1) * ($PXCi7 / $PCi1);
                echo $jumlah;?></td>
              </tr>
              <tr>
                <td>P(X|<b>STATUS KELULUSAN</b> = “TERLAMBAT”)</td>
                <td>= </td>
                <td>P(<b>KELAMIN</b> = “<?php echo $this->session->userdata('tbl_jk'); ?>”, <b>STATUS MAHASISWA</b> = “<?php echo $this->session->userdata('status_mhs'); ?>”, <b>STATUS PERNIKAHAN</b> = “<?php echo $this->session->userdata('status_pernikahan'); ?>”, <b>IPK</b> = <?php echo $this->session->userdata('ips_mhs'); ?> | <b>STATUS KELULUSAN</b> = “TERLAMBAT”)</td>
              </tr>
              <tr>
                <td></td>
                <td>=</td>
                <td><?php echo $PXCi2." / ".$PCi2." x ".$PXCi4." / ".$PCi2." x ".$PXCi6." / ".$PCi2." x ".$PXCi8." / ".$PCi2; ?></td>
              </tr>
              <tr>
                <td></td>
                <td>=</td>
                <td><?php $jumlah_terlambat = 0;
                $jumlah_terlambat = ($PXCi2 / $PCi2) * ($PXCi4 / $PCi2) * ($PXCi6 / $PCi2) * ($PXCi8 / $PCi2);
                echo $jumlah_terlambat;?></td>
              </tr>
            </table>
              <hr>
              <b>P(X|Ci)*P(Ci)</b><hr>
              <p>P(X|<b>STATUS KELULUSAN</b> = “TEPAT”)*P(<b>STATUS KELULUSAN</b> = “TEPAT”) = <?php echo $jumlah." x ".$PCi1." / ".$training;
              echo " = ".$hasil_tepat = $jumlah * ($PCi1/$training);?><br>
P(X|<b>STATUS KELULUSAN</b> = “TERLAMBAT”)*P(<b>STATUS KELULUSAN</b> = “TERLAMBAT”) = <?php echo $jumlah_terlambat." x ".$PCi2." / ".$training;
              echo " = ".$hasil_terlambat = $jumlah_terlambat * ($PCi2/$training);
              $status = '';
              if ($hasil_tepat > $hasil_tepat) {
                # code...
                echo "<br>".$hasil_tepat."<b> > </b>".$hasil_terlambat." => TEPAT";
                $status = "Tepat";
              }else{
                echo "<br>".$hasil_terlambat."<b> > </b>".$hasil_tepat." => TERLAMBAT";
                  $status = "Terlambat";
              }
              ?></p><hr>
              <b>Hasil :</b><hr>
              <p>Untuk <b>KELAMIN</b> = “<?php echo $this->session->userdata('tbl_jk'); ?>”, <b>STATUS MAHASISWA</b> = “<?php echo $this->session->userdata('status_mhs'); ?>”, <b>STATUS PERNIKAHAN</b> = “<?php echo $this->session->userdata('status_pernikahan'); ?>”, <b>IPK</b> = <?php echo $this->session->userdata('ips_mhs'); ?>, masuk ke kelas <b>STATUS KELULUSAN</b> = “<?php echo $status; 

              $data = array('tbl_jk' => $this->session->userdata('tbl_jk'),
                'status_mhs' => $this->session->userdata('status_mhs'),
                'status_pernikahan' => $this->session->userdata('status_pernikahan'),
                'ips_mhs' => $this->session->userdata('ips_mhs'),
                'status' => $status
                );
              $this->db->insert('tbl_testing', $data);
              ?>”</p>
            </div>
          </div>

        </div>
        <!-- /.container-fluid