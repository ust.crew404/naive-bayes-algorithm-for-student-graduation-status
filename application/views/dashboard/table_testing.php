        <div class="container-fluid">
        <?php 
          echo $this->session->userdata('notif'); 
          $this->session->set_userdata('notif',''); 
        ?>
          <!-- Page Heading -->
          <h1 class="h3 mb-2 text-gray-800">Data Testing</h1>

          <!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Input Data Testing</h6>
            </div>
            <div class="card-body">
            <form action="<?php echo base_url('data_testing/proses');?>" method="POST">
            <table width="100%">
             <tr>
                <td>Jenis Kelamin</td>
                <td>:</td>
                <td><select class="form-control form-control-user" name="tbl_jk" required="">
                  <option selected="" disabled="">-- Pilih --</option>
                  <option value="Laki - Laki">Laki - Laki</option>
                  <option value="Perempuan">Perempuan</option>
                </select></td>
              </tr>
              <tr>
                <td>Status Mahasiswa</td>
                <td>:</td>
                <td><select class="form-control form-control-user" name="status_mhs" required="">
                  <option selected="" disabled="">-- Pilih --</option>
                  <option value="Mahasiswa">Mahasiswa</option>
                  <option value="Bekerja">Bekerja</option>
                </select></td>
              </tr>
              <tr>
                <td>Status Pernikahan</td>
                <td>:</td>
                <td><select class="form-control form-control-user" name="status_pernikahan" required="">
                  <option selected="" disabled="">-- Pilih --</option>
                  <option value="Belum">Belum</option>
                  <option value="Menikah">Menikah</option>
                </select></td>
              </tr>
              <tr>
                <td>IPK Semester 1-6</td>
                <td>:</td>
                <td><input type="text" name="ips_mhs" class="form-control form-control-user" placeholder="IPK Semester 1-6" required=""></td>
              </tr>
              <tr>
                <td colspan="3"><br><center><input type="submit" class="btn btn-primary btn-user btn-block" value="Proses "></center></td>
              </tr>
            </table>
            </form>
              <hr>
            </div>
          </div>

        </div>
        <!-- /.container-fluid